<?php

namespace ShrooPHP\Core\Tests;

use ShrooPHP\Core\ArrayObject;
use ShrooPHP\Core\Filterable;
use PHPUnit\Framework\TestCase;

/**
 * A test case for \ShrooPHP\Core\ArrayObject.
 */
class ArrayObjectTest extends TestCase implements Filterable
{

	/**
	 * @var \ShrooPHP\Core\ArrayObject the array object being tested
	 */
	private $object;

	/**
	 * Asserts that the given filterable object has expected behavior via the
	 * given test case.
	 *
	 * @param \PHPUnit\Framework\TestCase $test the test case to use in order
	 * to assert
	 * @param Filterable $object the filterable object to test
	 */
	public static function assertFilter(TestCase $test, Filterable $object)
	{
		$filter = FILTER_VALIDATE_EMAIL;
		$address = 'test@example.org';
		$default = array('options' => array('default' => $address));

		$test->assertEquals(1, $object->filter('first'));
		$test->assertFalse($object->filter('first', $filter));

		$set = $object->filter('first', $filter, $default);
		$test->assertEquals($address, $set);

		$test->assertEquals('', $object->filter('second'));

		$unset = $object->filter('second', $filter, $default);
		$test->assertEquals($address, $unset);
	}

	public function filter($key, $filter = null, $options = null)
	{
		return ArrayObject::filterFrom($this->object, $key, $filter, $options);
	}

	/**
	 * Sets up each test by initializing the array object.
	 */
	public function setUp()
	{
		$this->object = new ArrayObject(array('first' => 1));
	}

	/**
	 * Asserts that values can be retrieved from an array object via the helper
	 * method as expected.
	 */
	public function testGetFrom()
	{
		$default = 'second';

		$this->assertEquals(1, ArrayObject::getFrom($this->object, 'first'));
		$this->assertNull(ArrayObject::getFrom($this->object, 2));

		$defaulted = ArrayObject::getFrom($this->object, 2, $default);
		$this->assertEquals($default, $defaulted);
	}

	/**
	 * Asserts that values can be filtered from a readable via the helper
	 * method as expected.
	 */
	public function testFilterFrom()
	{
		$filter = FILTER_VALIDATE_EMAIL;
		$address = 'test@example.org';
		$default = array('options' => array('default' => $address));

		$this->assertEquals(1, $this->filter('first'));
		$this->assertFalse($this->filter('first', $filter));

		$set = $this->filter('first', $filter, $default);
		$this->assertEquals($address, $set);

		$this->assertEquals('', $this->filter('second'));

		$unset = $this->filter('second', $filter, $default);
		$this->assertEquals($address, $unset);
	}

	/**
	 * Asserts that the readable and writable behavior is as expected.
	 */
	public function testGetAndSet()
	{
		$this->assertEquals(1, $this->object->get('first'));

		$this->assertNull($this->object->get(2));
		$this->assertEquals('null', $this->object->get(2, 'null'));

		$this->object->set(2, 'second');

		$this->assertEquals('second', $this->object->get(2));
	}

	/**
	 * Asserts that the filterable behavior is as expected.
	 */
	public function testFilter()
	{
		self::assertFilter($this, $this->object);
	}
}
