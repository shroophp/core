<?php

namespace ShrooPHP\Core\Tests\Runnables;

use ShrooPHP\Core\Runnables\ValueAdapter;
use PHPUnit\Framework\TestCase;

/**
 * A test case for ShrooPHP\Core\Runnables\ValueAdapter.
 */
class ValueAdapterTest extends TestCase
{
	/**
	 * @var ShrooPHP\Core\Runnables\ValueAdapter the adapter currently being
	 * tested
	 */
	private $adapter;

	/**
	 * @var mixed the value currently being adapted
	 */
	private $value;

	/**
	 * Sets up each test by initializing the current value and the current
	 * adapter.
	 */
	public function setUp()
	{
		$this->value = 'Hello, world!';
		$this->adapter = new ValueAdapter($this);
	}

	/**
	 * Asserts that the constructor of the adapter has expected behavior.
	 */
	public function testConstructor()
	{
		ob_start();
		$this->adapter->run();
		$this->assertEquals("{$this}", ob_get_clean());
	}

	/**
	 * Asserts that the setters of the adapter has expected behavior.
	 */
	public function testSetter()
	{
		$string = 'Goodbye, galaxy!';
		$adapter = new ValueAdapter($string);

		ob_start();
		$adapter->run();
		$this->assertEquals($string, ob_get_clean());

		$adapter->setValue($this);
		ob_start();
		$adapter->run();
		$this->assertEquals("{$this}", ob_get_clean());

	}

	/**
	 * Converts the current value to a string.
	 *
	 * @return string the current value as a string
	 */
	public function __toString()
	{
		return "{$this->value}";
	}
}

