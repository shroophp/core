<?php

namespace ShrooPHP\Core\Tests\Runnables;

use ShrooPHP\Core\Runnables\CallbackAdapter;
use PHPUnit\Framework\TestCase;

/**
 * A test case for \ShrooPHP\Core\Runnables\CallbackAdapter.
 */
class CallbackAdapterTest extends TestCase
{
	/**
	 * @var \ShrooPHP\Core\Runnables\CallbackAdapter the adapter currently
	 * being tested
	 */
	private $adapter;

	/**
	 * @var callable the callback currently being adapted
	 */
	private $callback;

	/**
	 * @var int the number of times that the test case has been ran during the
	 * current test
	 */
	private $ran;

	/**
	 * Increments the number of times that the test case has been ran.
	 */
	public function increment()
	{
		$this->ran++;
	}

	/**
	 * Sets up the test case by initializing the number of times that the test
	 * case has been ran, the callback and the adapter.
	 */
	public function setUp()
	{
		$this->ran = 0;
		$this->callback = array($this, 'increment');
		$this->adapter = new CallbackAdapter($this->callback);
	}

	/**
	 * Asserts the constructor of the adapter.
	 */
	public function testConstructor()
	{
		$this->adapter->run();
		$this->assertEquals(1, $this->ran);
	}

	/**
	 * Asserts the setters of the adapter.
	 */
	public function testSetter()
	{
		$callback = function () {
			// Do nothing.
		};
		$adapter = new CallbackAdapter($callback);

		$adapter->run();
		$this->assertEquals(0, $this->ran);

		$adapter->setCallback($this->callback);
		$adapter->run();
		$this->assertEquals(1, $this->ran);
	}
}
