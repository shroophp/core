<?php

namespace ShrooPHP\Core\Pairs;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Pair as IPair;
use stdClass;

/**
 * A test case for \ShrooPHP\Core\Pairs\Pair.
 */
class PairTest extends TestCase
{

	/**
	 * @var array the values being paired and tested
	 */
	private static $pairs;

	/**
	 * @var int the total number of pairs being tested
	 */
	private static $total;

	/**
	 * Sets up the class by populating and totalling the pairs of values to be
	 * tested.
	 */
	public static function setUpBeforeClass()
	{
		self::$pairs = array(
			false, true,
			0, 1,
			0.5, 1.5,
			'first', 'second',
			new stdClass, new stdClass
		);

		self::$total = count(self::$pairs) / 2;
	}

	/**
	 * Asserts the construction of a pair and the manipulation of the first and
	 * second values.
	 */
	public function testConstructorAndSetters()
	{

		$this->assertConstructorAndSetters(null, null);

		for ($i = 0; $i < self::$total; $i++) {

			$first = self::$pairs[$i * 2];

			for ($j = 0; $j < self::$total; $j++) {

				$second = self::$pairs[($j * 2) + 1];
				$this->assertConstructorAndSetters($first, $second);
				$this->assertConstructorAndSetters($second, $first);
			}
		}
	}

	/**
	 * Asserts that the given values can be constructed and manipulated within
	 * a pair.
	 *
	 * @param mixed $first the first value of the pair
	 * @param mixed $second the second value of the pair
	 */
	protected function assertConstructorAndSetters($first, $second)
	{

		$pair = new Pair($first, $second);
		$this->assertPair($pair, $first, $second);

		$pair->setFirst($second);
		$this->assertPair($pair, $second, $second);

		$pair->setSecond($first);
		$this->assertPair($pair, $second, $first);

		$pair->setFirst($first);
		$this->assertPair($pair, $first, $first);

		$pair->setSecond($second);
		$this->assertPair($pair, $first, $second);
	}

	/**
	 * Asserts that the given values form the given pair.
	 *
	 * @param \ShrooPHP\Core\Pair $pair the pair to test
	 * @param mixed $first the expected value of the first member
	 * @param mixed $second the expected value of the second member
	 */
	protected function assertPair(IPair $pair, $first, $second)
	{
		$this->assertSame($first, $pair->first());
		$this->assertSame($second, $pair->second());
	}
}
