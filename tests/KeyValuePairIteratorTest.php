<?php

namespace ShrooPHP\Core\Tests;

use ArrayIterator;
use ShrooPHP\Core\KeyValuePairIterator;
use ShrooPHP\Core\Pair as IPair;
use ShrooPHP\Core\Pairs\Pair;
use PHPUnit\Framework\TestCase;

class KeyValuePairIteratorTest extends TestCase
{
	/**
	 * the method name and its return value for the mock object
	 */
	const METHOD = '__toString';

	/**
	 * @var array the values being tested
	 */
	private static $values;

	/**
	 * @var int the number of values being tested
	 */
	private static $length;

	/**
	 * @var int the expected number of iterations
	 */
	private static $iterations;

	/**
	 * @var array the pairs being tested
	 */
	private $pairs = null;

	/**
	 * @var \ArrayIterator the pairs being tested as an iterator
	 */
	private $iterator = null;

	/**
	 * Sets up the class by populating the values to be tested and determining
	 * their number, and hence the number of expected iterations.
	 */
	public static function setUpBeforeClass()
	{
		self::$values = array(null, '', 0, 1, 0.5, 'string');
		self::$length = count(self::$values);
		self::$iterations = self::$length * self::$length;
	}

	/**
	 * Converts the given values to an array containing every possible
	 * permutation as a pair
	 *
	 * @param \Traversable|array $values the values to convert
	 * @return array every possible permutation as a pair
	 */
	protected static function toPairs($values)
	{
		$pairs = array();

		foreach ($values as $first) {
			foreach ($values as $second) {
				$pairs[] = new Pair($first, $second);
			}
		}

		return $pairs;
	}

	/**
	 * Sets up the current test by ensuring that the pairs and iterator have
	 * been instantiated.
	 */
	public function setUp()
	{
		if (is_null($this->pairs)) {

			$mock = $this->getMockBuilder('stdClass')
				->setMethods(array(self::METHOD))
				->getMock();

			$mock->method(self::METHOD)->willReturn(self::METHOD);

			$this->assertEquals(self::METHOD, "{$mock}");

			self::$values[] = $mock;
			self::$length++;
			self::$iterations = self::$length * self::$length;

			$this->pairs = self::toPairs(self::$values);

			if (is_null($this->iterator)) {
				$this->iterator = new ArrayIterator($this->pairs);
			}
		}
	}

	/**
	 * Asserts that iteration over an array via a foreach loop conforms to
	 * expected behaviour.
	 */
	public function testArrayForEach()
	{
		$this->assertForEachIteration(new KeyValuePairIterator($this->pairs));
	}

	/**
	 * Asserts that iteration over an iterator via a foreach loop conforms to
	 * expected behavior.
	 */
	public function testIteratorForEach()
	{
		$this->assertForEachIteration(new KeyValuePairIterator($this->iterator));
	}

	/**
	 * Asserts that iteration over an array via a while loop conforms to expected
	 * behavior.
	 */
	public function testArrayWhile()
	{
		$this->assertWhileIteration(new KeyValuePairIterator($this->pairs));
	}

	/**
	 * Asserts that iteration over an iterator via a while loop conforms to
	 * expected behavior.
	 */
	public function testIteratorWhile()
	{
		$this->assertWhileIteration(new KeyValuePairIterator($this->iterator));
	}

	/**
	 * Asserts that the iteration of the given iterator via a foreach loop
	 * conforms to expected behaviour.
	 *
	 * @param \KeyValuePairIterator $iterator the iterator to iterate over
	 */
	protected function assertForEachIteration(KeyValuePairIterator $iterator)
	{
		$pair = null;
		$i = 0;
		$j = 0;
		$iterations = 0;

		foreach ($iterator as $key => $value) {

			$pair = $this->toPair($i, $j, $pair);
			$this->assertPair($pair, $key, $value);
			$this->increment($i, $j);
			$iterations++;
		}

		$this->assertIterations($iterations);
	}


	/**
	 * Asserts that the iteration of the given iterator via a while loop
	 * conforms to expected behaviour.
	 *
	 * @param \KeyValuePairIterator $iterator the iterator to iterate over
	 */
	protected function assertWhileIteration(KeyValuePairIterator $iterator)
	{

		$pair = null;
		$i = 0;
		$j = 0;
		$iterations = 0;

		$iterator->rewind();

		while ($iterator->valid()) {

			$pair = $this->toPair($i, $j, $pair);
			$this->assertPair($pair, $iterator->key(), $iterator->current());
			$this->increment($i, $j);
			$iterations++;

			$iterator->next();
		}

		$this->assertIterations($iterations);
	}

	/**
	 * Converts the given set of scalars to a pair (or populates the given pair
	 * if specified).
	 *
	 * @param scalar $i the index of the first value
	 * @param scalar $j the index of the second value
	 * @param \ShrooPHP\Core\Pairs\Pair $pair if specified, the given pair
	 * will be populated instead of a new pair being instantiated
	 * @return \ShrooPHP\Core\Pair the given scalars as a pair
	 */
	protected function toPair($i, $j, Pair $pair = null)
	{

		$first = self::$values[$i];
		$second = self::$values[$j];


		if (is_null($pair)) {
			$pair = new Pair($first, $second);
		} else {
			$pair->setFirst($first);
			$pair->setSecond($second);
		}

		return $pair;
	}

	/**
	 * Asserts that the given pair is formed of the given values within the
	 * context on an iteration.
	 *
	 * @param \ShrooPHP\Core\Pair $pair the pair to assert (the value of
	 * which will be converted to a scalar)
	 * @param mixed $key the expected value of the first member
	 * @param mixed $value the expected value of the second member
	 */
	protected function assertPair(IPair $pair, $key, $value)
	{
		$first = $pair->first();

		if (is_float($first) || !is_scalar($first)) {
			$first = "{$first}";
		}

		$this->assertSame($first, $key);
		$this->assertSame($pair->second(), $value);
	}

	/**
	 * Asserts that the given number of total iterations is expected.
	 *
	 * @param int $iterations the number of total iterations to assert
	 */
	protected function assertIterations($iterations)
	{
		$this->assertEquals(
			self::$iterations,
			$iterations,
			'Unexpected number of iterations.'
		);
	}

	/**
	 * Increments the given indices being tested as a pair.
	 *
	 * @param int $i the value of the most significant index
	 * @param int $j the value of the least significant index
	 */
	protected function increment(&$i, &$j)
	{
		$j++;

		if ($j >= self::$length) {
			$j = 0;
			$i++;
		}
	}
}
