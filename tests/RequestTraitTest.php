<?php

namespace ShrooPHP\Core\Tests;

use ShrooPHP\Core\ReadOnlyArrayObject;
use ShrooPHP\Core\Request as IRequest;
use ShrooPHP\Core\RequestTrait;
use ShrooPHP\Core\Session;
use ShrooPHP\Core\Token;
use ShrooPHP\Core\Upload;
use PHPUnit\Framework\TestCase;

/**
 * A request for the request trait test.
 */
class Request implements IRequest, Session, Token, Upload
{

	public function method()
	{
		return 'method';
	}

	public function path()
	{
		return 'path';
	}

	public function params()
	{
		return new ReadOnlyArrayObject(array('param' => 'param'));
	}

	public function headers()
	{
		return new ReadOnlyArrayObject(array('header' => 'header'));
	}

	public function cookies()
	{
		return new ReadOnlyArrayObject(array('cookie' => 'cookie'));
	}

	public function session()
	{
		return $this;
	}

	public function open()
	{
		return STDIN;
	}

	public function data()
	{
		return new ReadOnlyArrayObject(array('data' => 'data'));
	}

	public function uploads()
	{
		return array($this);
	}

	public function args()
	{
		return new ReadOnlyArrayObject(array('arg' => 'arg'));
	}

	public function token()
	{
		return $this;
	}

	public function root()
	{
		return $this;
	}

	public function get($key, $default = null)
	{
		return $default;
	}

	public function set($key, $value)
	{
		// Do nothing.
	}

	public function commit()
	{
		// Do nothing.
	}

	public function expected()
	{
		return '';
	}

	public function valid()
	{
		return false;
	}

	public function name()
	{
		return 'name';
	}

	public function type()
	{
		return 'type';
	}

	public function size()
	{
		return 'size';
	}

	public function error()
	{
		return UPLOAD_ERR_OK;
	}
}

/**
 * A test case for \ShrooPHP\Core\RequestTrait.
 */
class RequestTraitTest extends TestCase
{
	use RequestTrait;

	/**
	 * @var \ShrooPHP\Core\Request the request being wrapped by the trait
	 */
	private $request;

	/**
	 * Sets up each test by initializing the properties of the request.
	 */
	public function setUp()
	{
		$this->request = new Request;
	}

	/**
	 * Asserts that the trait has expected behavior.
	 */
	public function test()
	{
		$this->assertEquals($this->request->method(), $this->method());
		$this->assertEquals($this->request->path(), $this->path());
		$this->assertEquals($this->request->params(), $this->params());
		$this->assertEquals($this->request->headers(), $this->headers());
		$this->assertEquals($this->request->cookies(), $this->cookies());
		$this->assertEquals($this->request->session(), $this->session());
		$this->assertSame($this->request->open(), $this->open());
		$this->assertEquals($this->request->data(), $this->data());
		$this->assertEquals($this->request->uploads(), $this->uploads());
		$this->assertEquals($this->request->args(), $this->args());
		$this->assertEquals($this->request->token(), $this->token());
		$this->assertSame($this->request->root(), $this->root());
	}

	protected function request()
	{
		return $this->request;
	}
}
