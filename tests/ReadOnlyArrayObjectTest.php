<?php

namespace ShrooPHP\Core\Tests;

use BadMethodCallException;
use ShrooPHP\Core\ReadOnlyArrayObject;
use PHPUnit\Framework\TestCase;

class ReadOnlyArrayObjectTest extends TestCase
{

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the array object being tested
	 */
	private $object;

	/**
	 * Sets up each test by initializing the read-only array object.
	 */
	public function setUp()
	{
		$this->object = new ReadOnlyArrayObject(array('first' => 1));
	}

	/**
	 * Asserts that values can be retrieved from a read-only array object as
	 * expected.
	 */
	public function testGet()
	{
		$this->assertEquals(1, $this->object->get('first'));

		$this->assertNull($this->object->get(2));
		$this->assertEquals('null', $this->object->get(2, 'null'));
	}

	/**
	 * Asserts that values can be filtered from a read-only array object.
	 */
	public function testFilter()
	{
		ArrayObjectTest::assertFilter($this, $this->object);
	}

	/**
	 * Asserts that methods that modify the read-only array object cannot be
	 * called.
	 */
	public function testReadOnly()
	{
		$methods = array(
			array('append',        array(null)),
			array('asort',         array()),
			array('exchangeArray', array(array())),
			array('ksort',         array()),
			array('natcasesort',   array()),
			array('natsort',       array()),
			array('offsetSet',     array(0, null)),
			array('offsetUnset',   array(0)),
			array('uasort',        array('')),
			array('uksort',        array('')),
		);

		foreach ($methods as $method) {

			$exception = null;
			$args = $method[1];

			try {
				call_user_func_array(array($this->object, $method[0]), $args);
			} catch (BadMethodCallException $exception) {
				// Do nothing (implicitly assign the exception to $exception).
			}

			$this->assertNotNull($exception);
		}
	}
}

