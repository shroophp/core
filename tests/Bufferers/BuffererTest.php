<?php

namespace ShrooPHP\Core\Tests\Bufferers;

use Exception;
use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Bufferers\Bufferer;
use ShrooPHP\Core\Runnables\CallbackAdapter;
use ShrooPHP\Core\Runnables\ValueAdapter;
use org\bovigo\vfs\vfsStream;

class BuffererTest extends TestCase
{
	public function test()
	{
		
		$root = vfsStream::setup()->url();
		$expected = 'Hello, world!';
		$runnable = new ValueAdapter($expected);
		$bufferer = new Bufferer("{$root}/buffer");
		$buffer = $bufferer->buffer($runnable);
		$actual = stream_get_contents($buffer);
		fclose($buffer);

		$this->assertEquals($expected, $actual);
	}

	public function testException()
	{
		$exception = null;
		$level = ob_get_level();
		$runnable = new CallbackAdapter([$this, 'startOutputBufferingAndThrowException']);
		$bufferer = new Bufferer;

		try {
			$bufferer->buffer($runnable);
		} catch (Exception $exception) {
			// DO nothing (implicitly assign the exception).
		}

		$this->assertNotNull($exception);
		$this->assertEquals($level, ob_get_level());
	}

	public function startOutputBufferingAndThrowException()
	{
		ob_start();
		throw new Exception;
	}
}
