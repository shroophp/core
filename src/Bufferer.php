<?php

namespace ShrooPHP\Core;

use ShrooPHP\Core\Runnable;

interface Bufferer
{
        /**
         * Runs the given object and captures its output.
         *
         * @param \ShrooPHP\Core\Runnable $runnable the object to run
         * @return resource the captured output
         */
        public function buffer(Runnable $runnable);
}

