<?php

namespace ShrooPHP\Core;

use ArrayObject as SplArrayObject;
use BadMethodCallException;
use ShrooPHP\Core\ArrayObject;
use ShrooPHP\Core\Filterable;
use ShrooPHP\Core\Readable;

/**
 * An array object whose values may be filtered and read.
 */
class ReadOnlyArrayObject extends SplArrayObject implements Filterable, Readable
{

	public function filter($key, $filter = null, $options = null)
	{
		return ArrayObject::filterFrom($this, $key, $filter, $options);
	}

	public function get($key, $default = null)
	{
		return ArrayObject::getFrom($this, $key, $default);
	}

	public function append($value)
	{
		throw new BadMethodCallException;
	}

	public function asort($flags = SORT_REGULAR)
	{
		throw new BadMethodCallException;
	}

	public function exchangeArray($input)
	{
		throw new BadMethodCallException;
	}

	public function ksort($flags = SORT_REGULAR)
	{
		throw new BadMethodCallException;
	}

	public function natcasesort()
	{
		throw new BadMethodCallException;
	}

	public function natsort()
	{
		throw new BadMethodCallException;
	}

	public function offsetSet($index, $newval)
	{
		throw new BadMethodCallException;
	}

	public function offsetUnset($index)
	{
		throw new BadMethodCallException;
	}

	public function uasort($cmp_function)
	{
		throw new BadMethodCallException;
	}

	public function uksort($cmp_function)
	{
		throw new BadMethodCallException;
	}
}
