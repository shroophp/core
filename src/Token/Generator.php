<?php

namespace ShrooPHP\Core\Token;

/**
 * A generator of tokens.
 */
interface Generator
{
	/**
	 * Generates a token.
	 *
	 * @return string the generated token
	 */
	public function generate();
}
