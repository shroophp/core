<?php

namespace ShrooPHP\Core\Pattern;

/**
 * An interpreter of patterns.
 */
interface Interpreter
{
	/**
	 * Interprets the given pattern.
	 *
	 * @param string $pattern the pattern to interpret
	 * @return \ShrooPHP\Core\Pattern\Interpretation an interpretation of
	 * the given pattern
	 */
	public function interpret($pattern);
}
