<?php

namespace ShrooPHP\Core\Pattern;

/**
 * An interpretation of a pattern.
 */
interface Interpretation
{
	/**
	 * Converts the given subject to an array (if it matches the pattern).
	 *
	 * @param string $subject the subject to convert
	 * @return array|null the given subject as a matched pattern (or NULL if
	 * subject does not match)
	 */
	public function match($subject);
}
