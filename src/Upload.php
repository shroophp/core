<?php

namespace ShrooPHP\Core;

/**
 * An upload via PHP.
 */
interface Upload
{
	/**
	 * Gets the name of the upload.
	 *
	 * @return string the name of the upload
	 */
	public function name();

	/**
	 * Gets the type of the upload.
	 *
	 * @return string the type of the upload
	 */
	public function type();

	/**
	 * Gets the size of the upload.
	 *
	 * @return int the size of the upload
	 */
	public function size();

	/**
	 * Gets the temporary name of the upload.
	 *
	 * @return string the temporary name of the upload
	 */
	public function path();

	/**
	 * Gets the error associated with the upload.
	 *
	 * @return int the error associated with the upload
	 */
	public function error();
}

