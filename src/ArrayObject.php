<?php

namespace ShrooPHP\Core;

use ArrayObject as SplArrayObject;
use ShrooPHP\Core\Filterable;
use ShrooPHP\Core\Readable;
use ShrooPHP\Core\Writable;

/**
 * An array object whose values may be filtered, read, and written.
 */
class ArrayObject extends SplArrayObject implements Filterable, Readable, Writable
{
	/**
	 * Filters the value associated with the given key from the given object.
	 *
	 * @param \ShrooPHP\Core\Readable $obj the object to filter from
	 * @param scalar $key the key to filter the associated value of
	 * @param int $filter the ID of the filter to apply
	 * @params mixed the options associated with the filter
	 * @return mixed the filtered value (or FALSE if the filter fails)
	 * @see https://php.net/manual/book.filter.php
	 */
	public static function filterFrom(
		Readable $obj,
		$key,
		$filter = null,
		$options = null
	) {

		if (is_null($filter)) {
			$filter = FILTER_DEFAULT;
		}

		if (is_null($options)) {
			$options = array();
		}

		return filter_var($obj->get($key), $filter, $options);
	}

	/**
	 * Gets the value associated with the given key within the given array
	 * object.
	 *
	 * @param \ArrayObject $obj the array object to retrieve from
	 * @param scalar $key the key to read the associated value of
	 * @param mixed $default the value to return if the given key is NULL or
	 * not set
	 * @return mixed the value associated with the given key
	 */
	public static function getFrom(SplArrayObject $obj, $key, $default = null)
	{
		$value = null;

		if ($obj->offsetExists($key)) {
			$value = $obj->offsetGet($key);
		}

		if (is_null($value)) {
			$value = $default;
		}

		return $value;
	}

	public function filter($key, $filter = null, $options = null)
	{
		return self::filterFrom($this, $key, $filter, $options);
	}

	public function get($key, $default = null)
	{
		return self::getFrom($this, $key, $default);
	}

	public function set($key, $value)
	{
		$this->offsetSet($key, $value);
	}

}