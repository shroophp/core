<?php

namespace ShrooPHP\Core;

/**
 * An object that can be ran.
 */
interface Runnable
{
	/**
	 * Runs the object.
	 */
	public function run();
}
