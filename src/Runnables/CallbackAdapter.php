<?php

namespace ShrooPHP\Core\Runnables;

use ShrooPHP\Core\Runnable;

/**
 * An adapter for converting callbacks to runnable instances.
 */
class CallbackAdapter implements Runnable
{
	/**
	 * @var callable the callback being adapted
	 */
	private $callback;

	/**
	 * Constructs an adapter for the given callback so that it is runnable.
	 *
	 * @param callable $callback the callback to adapt
	 */
	public function __construct($callback)
	{
		$this->setCallback($callback);
	}

	/**
	 * Sets the callback being adapted.
	 *
	 * @param callable $callback the callback to adapt
	 */
	public function setCallback($callback)
	{
		$this->callback = $callback;
	}

	public function run()
	{
		call_user_func($this->callback);
	}
}