<?php

namespace ShrooPHP\Core\Runnables;

use ShrooPHP\Core\Runnable;

/**
 * An adapter for converting a primitive value into a runnable instance that
 * prints the value when ran.
 */
class ValueAdapter implements Runnable
{
	/**
	 * @var mixed the value being adapted
	 */
	private $value;

	/**
	 * Constructs an adapter for the given value so that in can be printed when
	 * ran.
	 *
	 * @param mixed $value the value to adapt
	 */
	public function __construct($value)
	{
		$this->setValue($value);
	}

	/**
	 * Sets the value being adapted.
	 *
	 * @param mixed $value the value to adapt
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}

	public function run()
	{
		echo $this->value;
	}
}