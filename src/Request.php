<?php

namespace ShrooPHP\Core;

use ShrooPHP\Core\Openable;

/**
 * A request.
 */
interface Request extends Openable
{

	/**
	 * Gets the method of the request.
	 *
	 * @return string the method of the request
	 */
	public function method();

	/**
	 * Gets the path of the request.
	 *
	 * @return string the path of the request
	 */
	public function path();

	/**
	 * Gets the URL parameters of the request.
	 *
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the URL parameters of the
	 * request
	 */
	public function params();

	/**
	 * Gets the headers of the request.
	 *
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the headers of the request
	 */
	public function headers();

	/**
	 * Gets the cookies of the request.
	 *
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the cookies of the request
	 */
	public function cookies();

	/**
	 * Gets the session associated with the request.
	 *
	 * @return \ShrooPHP\Core\Session the session associated with the request
	 */
	public function session();

	/**
	 * Gets the data associated with the body of the request.
	 *
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the data associated with the
	 * body of the request
	 */
	public function data();

	/**
	 * Gets the files associated with the request.
	 *
	 * @return array the files associated with the request
	 */
	public function uploads();

	/**
	 * Gets the arguments associated with the request.
	 *
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the arguments associated with
	 * the request
	 */
	public function args();

	/**
	 * Gets the token associated with the request.
	 *
	 * @return \ShrooPHP\Core\Token the token associated with the request
	 */
	public function token();

	/**
	 * Gets the greatest ancestor of this request (or the current request
	 * if there are no descendants.
	 *
	 * @return \ShrooPHP\Core\Request the greatest ancestor of the request
	 * (or the current request if there are no descendants.
	 */
	public function root();
}
