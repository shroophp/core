<?php

namespace ShrooPHP\Core;

use ShrooPHP\Core\ReadOnlyArrayObject;

/**
 * An immutable array object.
 */
class ImmutableArrayObject extends ReadOnlyArrayObject
{

}
