<?php

namespace ShrooPHP\Core;

/**
 * A token being used for authentication.
 */
interface Token
{
	/**
	 * Gets the expected token.
	 *
	 * @return string the expected token
	 */
	public function expected();

	/**
	 * Determines whether or not the token is valid.
	 *
	 * @return bool whether or not the token is valid
	 */
	public function valid();
}

