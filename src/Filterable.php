<?php

namespace ShrooPHP\Core;

/**
 * An object with dynamic properties that can be filtered.
 */
interface Filterable
{
	/**
	 * Filters the value associated with the given key.
	 *
	 * @param scalar $key the key to filter the associated value of
	 * @param int $filter the ID of the filter to apply
	 * @params mixed the options associated with the filter
	 * @return mixed the filtered value (or FALSE if the filter fails)
	 * @see https://php.net/manual/book.filter.php
	 */
	public function filter($key, $filter = null, $options = null);
}
