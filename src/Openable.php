<?php

namespace ShrooPHP\Core;

/**
 * An object that opens file pointer resources.
 */
interface Openable
{

	/**
	 * Opens a file pointer resource.
	 *
	 * @returns resource|null a file pointer resource (or NULL on failure)
	 */
	public function open();
}
