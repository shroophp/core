<?php

namespace ShrooPHP\Core\Bufferers;

use Exception;
use ShrooPHP\Core\Bufferer as IBufferer;
use ShrooPHP\Core\Runnable;

class Bufferer implements IBufferer
{

	const BUFFER = 'php://temp';

	const LENGTH = 0x1000;

	private $buffer;

	private $length;

	public function __construct($buffer = null, $length = null)
	{
		if (is_null($buffer)) {
			$buffer = self::BUFFER;
		}

		if (is_null($length)) {
			$length = self::LENGTH;
		}

		$this->buffer = $buffer;
		$this->length = $length;
	}

	public function buffer(Runnable $runnable)
	{
		$handle = fopen($this->buffer, 'w+b');
		$level = ob_get_level();

		try {
			ob_start(function ($string) use ($handle) {
				fwrite($handle, $string);
				return '';
			}, $this->length);

			$runnable->run();
			ob_end_clean();

		} catch (Exception $e) {

			while (ob_get_level() > $level) {
				ob_end_clean();
			}

			throw $e;
		}

		fseek($handle, 0);

		return $handle;

	}
}
