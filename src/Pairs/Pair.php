<?php

namespace ShrooPHP\Core\Pairs;

use ShrooPHP\Core\Pair as IPair;

/**
 * A pair of values.
 */
class Pair implements IPair
{

	/**
	 * @var mixed the first value of the pair
	 */
	private $first;

	/**
	 * @var mixed the second value of the pair
	 */
	private $second;

	/**
	 * Constructs a pair.
	 *
	 * @param mixed $first the first value of the pair
	 * @param mixed $second the second value of the pair
	 */
	public function __construct($first, $second)
	{
		$this->setFirst($first);
		$this->setSecond($second);
	}

	/**
	 * Sets the first value of the pair.
	 *
	 * @param mixed $first the first value of the pair
	 */
	public function setFirst($first)
	{
		$this->first = $first;
	}

	/**
	 * Sets the second value of the pair.
	 *
	 * @param mixed $second the second value of the pair
	 */
	public function setSecond($second)
	{
		$this->second = $second;
	}

	public function first()
	{
		return $this->first;
	}

	public function second()
	{
		return $this->second;
	}
}
