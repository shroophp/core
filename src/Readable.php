<?php

namespace ShrooPHP\Core;

/**
 * An object with dynamic properties that can be read.
 */
interface Readable
{

	/**
	 * Gets the value associated with the given key.
	 *
	 * @param scalar $key the key to read the associated value of
	 * @param mixed $default the value to return if the given key is NULL or
	 * not set
	 * @return mixed the value associated with the given key
	 */
	public function get($key, $default = null);
}
