<?php

namespace ShrooPHP\Core;

/**
 * A pair of values.
 */
interface Pair
{

	/**
	 * Gets the first value of the pair.
	 * 
	 * @return mixed the first value of the pair
	 */
	public function first();

	/**
	 * Gets the second value of the pair.
	 * 
	 * @return mixed the second value of the pair
	 */
	public function second();
}
