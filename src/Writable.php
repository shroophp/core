<?php

namespace ShrooPHP\Core;

/**
 * An object with dynamic properties that can be set.
 */
interface Writable
{

	/**
	 * Sets the value associated with the given key.
	 *
	 * @param scalar $key the key to set the value of
	 * @param mixed $value the value to associate with the given key
	 */
	public function set($key, $value);
}
