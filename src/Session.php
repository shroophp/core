<?php

namespace ShrooPHP\Core;

use ShrooPHP\Core\Readable;
use ShrooPHP\Core\Writable;

/**
 * A repository of data associated with the session of the request.
 */
interface Session extends Readable, Writable
{
	/**
	 * Commits all existing changes to the session.
	 */
	public function commit();
}
