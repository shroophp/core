<?php

namespace ShrooPHP\Core\Request;

/**
 * A response to a request.
 */
interface Response
{

	/**
	 * Gets the status code of the response (if any).
	 *
	 * @return int|null the status code of the response (if any)
	 */
	public function code();

	/**
	 * Gets the headers of the response.
	 *
	 * @return \Traversable the headers of the response
	 */
	public function headers();

	/**
	 * Gets the content of the response.
	 *
	 * @return \ShrooPHP\Core\Runnable|null the content of the response
	 * (if any)
	 */
	public function content();

}
