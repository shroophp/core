<?php

namespace ShrooPHP\Core\Request;

use ShrooPHP\Core\Request;

/**
 * A validator of requests.
 */
interface Validator
{
	/**
	 * Validates the given request.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to validate
	 * @return bool whether or not the request is valid
	 */
	public function validate(Request $request);
}

