<?php

namespace ShrooPHP\Core\Request;

use ShrooPHP\Core\Request;

/**
 * A handler for requests.
 */
interface Handler
{
	/**
	 * Handles the given request.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to handle
	 * @return bool TRUE if the given request was handled (FALSE otherwise)
	 */
	public function handle(Request $request);
}
