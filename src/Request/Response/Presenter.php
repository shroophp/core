<?php

namespace ShrooPHP\Core\Request\Response;

use ShrooPHP\Core\Request\Response;

/**
 * A presenter of responses.
 */
interface Presenter
{

	/**
	 * Presents the given response.
	 *
	 * @param \ShrooPHP\Core\Request\Response $response the response to present
	 */
	public function present(Response $response);
}
