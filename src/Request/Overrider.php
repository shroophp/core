<?php

namespace ShrooPHP\Core\Request;

use ShrooPHP\Core\Request;

/**
 * A overrider of requests.
 */
interface Overrider
{
	/**
	 * Overrides the given request.
	 *
	 * @param  \ShrooPHP\Core\Request $request the request to override
	 * @return \ShrooPHP\Core\Request the overridden request
	 */
	public function override(Request $request);
}
