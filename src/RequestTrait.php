<?php

namespace ShrooPHP\Core;

/**
 * Wrapper methods for classes implementing the \ShrooPHP\Core\Request
 * interface.
 */
trait RequestTrait
{
	/**
	 * Gets the method of the request.
	 *
	 * @return string the method of the request
	 */
	public function method()
	{
		return $this->request()->method();
	}

	/**
	 * Gets the path of the request.
	 *
	 * @return string the path of the request
	 */
	public function path()
	{
		return $this->request()->path();
	}

	/**
	 * Gets the URL parameters of the request.
	 *
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the URL parameters of the
	 * request
	 */
	public function params()
	{
		return $this->request()->params();
	}

	/**
	 * Gets the headers of the request.
	 *
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the headers of the request
	 */
	public function headers()
	{
		return $this->request()->headers();
	}

	/**
	 * Gets the cookies of the request.
	 *
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the cookies of the request
	 */
	public function cookies()
	{
		return $this->request()->cookies();
	}

	/**
	 * Gets the session associated with the request.
	 *
	 * @return \ShrooPHP\Core\Session the session associated with the request
	 */
	public function session()
	{
		return $this->request()->session();
	}

	/**
	 * Opens a file pointer resource for the body of the request.
	 *
	 * @return resource a file pointer resource for the body of the request
	 */
	public function open()
	{
		return $this->request()->open();
	}

	/**
	 * Gets the data associated with the body of the request.
	 *
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the data associated with the
	 * body of the request
	 */
	public function data()
	{
		return $this->request()->data();
	}

	/**
	 * Gets the files associated with the request.
	 *
	 * @return array the files associated with the request
	 */
	public function uploads()
	{
		return $this->request()->uploads();
	}

	/**
	 * Gets the arguments associated with the request.
	 *
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the arguments associated with
	 * the request
	 */
	public function args()
	{
		return $this->request()->args();
	}

	/**
	 * Gets the token associated with the request.
	 *
	 * @return \ShrooPHP\Core\Token the token associated with the request
	 */
	public function token()
	{
		return $this->request()->token();
	}

	/**
	 * Gets the greatest ancestor of this request (or the current request
	 * if there are no descendants.
	 *
	 * @return \ShrooPHP\Core\Request the greatest ancestor of the request
	 * (or the current request if there are no descendants.
	 */
	public function root()
	{
		return $this->request()->root();
	}

	/**
	 * Gets the request.
	 *
	 * @return \ShrooPHP\Core\Request the request
	 */
	protected abstract function request();

}
