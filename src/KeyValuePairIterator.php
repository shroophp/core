<?php

namespace ShrooPHP\Core;

use ArrayIterator;
use IteratorIterator;

/**
 * Iterates over a collection of pairs so as to return the first value as the
 * key and the second value as the current value.
 *
 * If the first value of the pair is not a scalar, it will be converted to a
 * string so as to be a valid key.
 */
class KeyValuePairIterator extends IteratorIterator
{
	/**
	 * Converts the given iterable value to an iterator appropriate for the
	 * parent constructor.
	 *
	 * @param \Traversable|array $iterator the iterable value to convert
	 *
	 * @return \Traversable the converted value
	 */
	private static function toIterator($iterator)
	{
		if (is_array($iterator)) {
			$iterator = new ArrayIterator($iterator);
		}

		return $iterator;
	}

	/**
	 * Constructs a key value pair iterator.
	 *
	 * @param \Traversable|array $iterator the collection of pairs to iterate
	 * over
	 */
	public function __construct($iterator)
	{
		parent::__construct(self::toIterator($iterator));
	}

	public function key()
	{
		$key = parent::current()->first();

		if (is_float($key) || !is_scalar($key)) {
			$key = "{$key}";
		}

		return $key;
	}

	public function current()
	{
		return parent::current()->second();
	}
}
