# ShrooPHP\Core

Core interfaces and functionality for libraries and applications relating to
ShrooPHP.

## Installation

```
composer require 'shroophp/core ^2.2'
```
